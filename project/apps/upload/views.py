from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.core.files.storage import FileSystemStorage
from django.urls import reverse
from django.views.generic import CreateView, TemplateView, ListView, UpdateView
from .models import Image
from .forms import ImageForm


# Create your views here.


class Home(ListView):
    model = Image
    template_name = 'home_page.html'


class ImageUploadView(CreateView):
    model = Image
    form_class = ImageForm
    template_name = 'images/image_upload.html'

    def get_success_url(self):
        return reverse('image', args=(self.object.id,))


class ImageView(UpdateView):
    model = Image
    form_class = ImageForm

    template_name = 'images/image.html'
    context_object_name = 'image'

    def get_success_url(self):
        return reverse('image', args=(self.object.id,))
