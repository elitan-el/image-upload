from django.db import models


class Image(models.Model):
    title = models.CharField(max_length=150)
    image = models.ImageField(upload_to='images/%Y.%m.%d/')
    image_width = models.PositiveIntegerField(default='Image.image.image_width', editable=True)
    image_height = models.PositiveIntegerField(default='Image.image.image_height', editable=True)

    class Meta:
        verbose_name = u'Изображение'
        verbose_name_plural = u"Изображения"

    def __str__(self):
        return self.title
