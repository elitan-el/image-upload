# Тестовое проект-задание для вакансии [Python разработчик](https://hh.ru/vacancy/46976315?utm_source=email&utm_medium=email&utm_campaign=company_interested&utm_content=text&sent_date=2021_08_18) от [Idaproject](https://idaproject.notion.site/backend-14c451038c5541c9996095192db75fc6)

[Описание задания](https://idaproject.notion.site/backend-14c451038c5541c9996095192db75fc6)

---

## Инструкция по разворачиванию проекта

### Установка и активация виртуального окружения

```
python -m venv .venv
.venv\Scripts\activate
```

### Установка зависимостей из файла requiremnts.txt 
> Сам файл создаётся командой ```pip freeze > requiremnts.txt```

```
pip install -r requiremnts.txt
```

### Запуск проекта
#### Переходим в папку с проектом
```
cd project
```
#### Миграции
```
python manage.py migrate
```
#### Создание супер пользователя
```
python manage.py createsuperuser
```
#### Запуск
```
python manage.py runserver 127.0.0.1:8000
```

#### 
```

```

#### 
```

```